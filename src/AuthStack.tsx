import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './screens/loginScreen';
import RegisterScreen from './screens/registerScreen';
import { AuthParamList } from './AuthParamList';

const Stack = createStackNavigator<AuthParamList>();

interface AuthStackProps {

}


export const AuthStack: React.FC<AuthStackProps> = ({ }) => {
    return (
        < Stack.Navigator initialRouteName="Login">
            <Stack.Screen options={{ headerTitle: "Sign In" }} name="Login" component={LoginScreen} />
            <Stack.Screen options={{ headerTitle: "Sign Up" }} name="Register" component={RegisterScreen} />
        </Stack.Navigator>
    )
}