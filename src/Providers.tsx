import React from 'react';
import { AuthProvider } from './AuthProvider';
import { Routes } from './Routes';

interface PrivdersProps {

}

export const Providers: React.FC<PrivdersProps> = ({ }) => {
    return (
        <AuthProvider>
            <Routes />
        </AuthProvider>
    );
}