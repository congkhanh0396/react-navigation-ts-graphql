import React, { useContext } from 'react';
import { Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import FeedScreen from './screens/feedScreen';
import { HomeParamList } from './HomeParamList';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AuthContext } from './AuthProvider';
import { Ionicons } from '@expo/vector-icons';
const Stack = createStackNavigator<HomeParamList>();

interface HomeStackProps {

}

export const HomeStack: React.FC<HomeStackProps> = ({ }) => {
    const { logout } = useContext(AuthContext);
    return (
        <Stack.Navigator>
            <Stack.Screen options={{
                headerRight: () => {
                    return (
                        <TouchableOpacity
                            onPress={() => {
                                logout();
                            }}
                        >
                            <Ionicons name="exit-outline" size={22} />
                        </TouchableOpacity>
                    );
                }
            }} name="Feed" component={FeedScreen} />
        </Stack.Navigator>
    )
}