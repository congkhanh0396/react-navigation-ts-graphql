import React, { useContext } from 'react';
import { Button, Text } from 'react-native';
import { AuthContext } from '../AuthProvider';
import { Center } from '../Center';

const Home = () => {
    const { logout } = useContext(AuthContext);
    return (
        <Center>
            <Text>Home</Text>
            <Button title="Log out" onPress={()=> logout()}/>
        </Center>
    )
}

export default Home;