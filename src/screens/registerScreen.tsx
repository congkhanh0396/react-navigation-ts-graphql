import React from 'react';
import { Text, Button } from 'react-native';
import { Center } from '../Center';
import {AuthNavProps} from '../AuthParamList';

// type ProfileScreenNavigationProp = StackNavigationProp<
//     AuthParamList,
//     'Register'
// >;


// type Props = {
//     navigation: StackNavigationProp<AuthParamList, 'Register'>;
//     route: RouteProp<AuthParamList, "Register">
// };


const register = ({ navigation, route }: AuthNavProps<"Register">) => {
    return (
        <Center>
            <Text>{route.name}</Text>
            {/* <Button title="Go to login" onPress={() => navigation.navigate('Login')} /> */}
        </Center>
    )
}

export default register;
