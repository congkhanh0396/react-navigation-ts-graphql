import React, { useContext } from 'react';
import { Text, Button } from 'react-native';
import { Center } from '../Center';
import { AuthNavProps } from '../AuthParamList'
import { AuthContext } from '../AuthProvider';

// cách định dạng thứ 1

// type ProfileScreenNavigationProp = StackNavigationProp<
//   AuthParamList,
//   'Login'
// >;

// cách định dạng thứ 2

// type Props = {
//     navigation: StackNavigationProp<AuthParamList, 'Login'>;
//     route: RouteProp<AuthParamList, 'Login'>
// };


function login({ navigation, route }: AuthNavProps<"Login">) {
    const {login} = useContext(AuthContext);
    return (
        <Center>
            <Text>{route.name}</Text>
            <Button title="Let me log in" onPress={() => {login();}} />
            <Button title="Register" onPress={() => {navigation.navigate("Register")}} />
        </Center>
    )
}



export default login;