import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AppParamList } from './AppParamList';
import HomeScreen from './screens/homeScreen';
import SearchScreen from './screens/searchScreen';
import { HomeStack } from './HomeStack';
import { AntDesign, Ionicons, EvilIcons } from '@expo/vector-icons';

interface AppTabsProps {

}

const Tabs = createBottomTabNavigator<AppParamList>();


export const AppTabs: React.FC<AppTabsProps> = ({ }) => {
    return (
        <Tabs.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Home') {
                        iconName = "home";
                        return <AntDesign name={"home"} size={size} color={color} />;
                    } else if (route.name === 'Search') {
                        iconName = "search";
                        return <EvilIcons name={"search"} size={size} color={color} />;
                    }
                    return <Ionicons name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'gray',
            }}
        >
            <Tabs.Screen name="Home" component={HomeStack} />
            <Tabs.Screen name="Search" component={SearchScreen} />
        </Tabs.Navigator>
    )
}