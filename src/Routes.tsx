import React, { useState, useEffect, useContext } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { Center } from './Center';
import { ActivityIndicator, Text } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AuthContext } from './AuthProvider';
import { AppTabs } from './AppTabs';
import { AuthStack } from './AuthStack';


interface RoutesProps { }


export const Routes: React.FC<RoutesProps> = ({ }) => {
    const { user, login } = useContext(AuthContext);
    const [loading, setloading] = useState<Boolean>(true);

    // check Login or not Login
    useEffect(() => {
        AsyncStorage.getItem('user')
            .then(userString => {
                if (userString) {
                    login();
                }
                setloading(false);
            })
            .catch(err => {
                console.log(err);
                console.log('catch');
                setloading(false);
            })
    }, [])

    console.log('loading', loading);

    if (loading) {
        return (
            <Center>
                <ActivityIndicator size="large" />
            </Center>
        )
    }

    return (
        <NavigationContainer>
            {user ? (<AppTabs />) : (<AuthStack />)}
        </NavigationContainer >
    );
}